# Nats.io and Streaming server in a docker environment #

A simple [nats.io](https://nats.io/) and [streaming server](https://github.com/nats-io/nats-streaming-server) container environment with docker

### How to use this? ###

Use build.sh script to set up the environment

This environment has 3 streaming server nodes with nats embedded each one