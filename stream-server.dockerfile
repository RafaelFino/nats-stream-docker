from centos

ADD ./scripts/get-nats-stream.sh /bin/

RUN yum install unzip telnet vim wget curl -y \
    && get-nats-stream.sh

EXPOSE 4222 5222 6222 8222

ADD ./config/*.config /etc/nats/