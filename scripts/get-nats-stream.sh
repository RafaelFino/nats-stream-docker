#!/bin/bash

mkdir /opt/nats
wget https://github.com/nats-io/nats-streaming-server/releases/download/v0.11.2/nats-streaming-server-v0.11.2-linux-amd64.zip -O /opt/nats/nats-stream.zip
unzip /opt/nats/nats-stream -d /opt/nats/

mv /opt/nats/nats-streaming-server-v0.11.2-linux-amd64/nats-streaming-server /opt/nats

rm -rf /opt/nats/nats-stream.zip
rm -rf /opt/nats/nats-streaming-server-v0.11.2-linux-amd64